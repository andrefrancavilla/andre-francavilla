using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Module : MonoBehaviour
{
    [System.Serializable]
    public struct Wall
    {
        public WallType type;
        public WallPosition position;

        public Wall(WallType type, WallPosition position)
        {
            this.type = type;
            this.position = position;
        }
    }

    public Vector2Int moduleCoordinates;
    public List<Wall> wallsPresentInModule;
    
    //Internal
    private GameObject wallParent = null;

    public void PlaceWall(Wall wall)
    {
        switch (wall.type)
        {
            case WallType.WallWithDoor:
                SpawnAtPosition(LevelGenerator.Instance.generatorResources.wallWithDoor, wall.position);
                
                break;
            case WallType.WallWithoutDoor:
                SpawnAtPosition(LevelGenerator.Instance.generatorResources.wallWithoutDoor, wall.position);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        wallsPresentInModule.Add(wall);
        HandleAdjacentWalls();
    }

    void SpawnAtPosition(GameObject prefab, WallPosition position)
    {
        if (WallPositionIsOccupied(position))
        {
            return;
        }
        
        //Set wall parent  if not present
        wallParent ??= new GameObject();
        wallParent.transform.parent = transform;
        wallParent.transform.localPosition = Vector3.zero;
        wallParent.transform.localScale = LevelGenerator.Instance.generatorResources.moduleSize;
        
        //Configure coordinates
        Vector3Int localSpawnPosition = Vector3Int.zero;
        Quaternion spawnRotation = Quaternion.identity;

        switch (position)
        {
            case WallPosition.Bottom:
                localSpawnPosition = new Vector3Int(0, 0, -1);
                spawnRotation = Quaternion.identity;
                break;
            case WallPosition.Top:
                localSpawnPosition = new Vector3Int(0, 0, 0);
                spawnRotation = Quaternion.identity;
                break;
            case WallPosition.Left:
                localSpawnPosition = new Vector3Int(0, 0, 0);
                spawnRotation = Quaternion.Euler(new Vector3(0, 90, 0));
                break;
            case WallPosition.Right:
                localSpawnPosition = new Vector3Int(1, 0, 0);
                spawnRotation = Quaternion.Euler(new Vector3(0, 90, 0));
                break;
        }

        GameObject wallClone = Instantiate(prefab);
        wallClone.transform.parent = wallParent.transform;
        wallClone.transform.localPosition = localSpawnPosition;
        wallClone.transform.localRotation = spawnRotation;
    }

    public bool WallPositionIsOccupied(WallPosition position, bool log = true)
    {
        for (int i = 0; i < wallsPresentInModule.Count; i++)
        {
            if (wallsPresentInModule[i].position != position) continue;
            
            if(log)
                Debug.Log($"Wall position {position} is occupied by {wallsPresentInModule[i].type} for module {gameObject.name}");
            return true;
        }

        return false;
    }

    /// <summary>
    /// Adds a wall to the list without actually spawning it.
    /// </summary>
    /// <param name="wall"></param>
    public void AssignWallIfNotOccupied(Wall wall)
    {
        if(WallPositionIsOccupied(wall.position, false)) return;
        
        wallsPresentInModule.Add(wall);
    }

    public void HandleAdjacentWalls()
    {
        foreach (Wall wall in wallsPresentInModule)
        {
            switch (wall.position)
            {
                case WallPosition.Top:
                    if(moduleCoordinates.y < LevelGenerator.Instance.levelSize.y - 1)
                        LevelGenerator.Instance.spawnedModules[moduleCoordinates.x, moduleCoordinates.y + 1].AssignWallIfNotOccupied(new Wall(wall.type, WallPosition.Bottom));
                    break;
                case WallPosition.Bottom:
                    if(moduleCoordinates.y > 0)
                        LevelGenerator.Instance.spawnedModules[moduleCoordinates.x, moduleCoordinates.y - 1].AssignWallIfNotOccupied(new Wall(wall.type, WallPosition.Top));
                    break;
                case WallPosition.Left:
                    if(moduleCoordinates.x > 0)
                        LevelGenerator.Instance.spawnedModules[moduleCoordinates.x - 1, moduleCoordinates.y].AssignWallIfNotOccupied(new Wall(wall.type, WallPosition.Right));
                    break;
                case WallPosition.Right:
                    if(moduleCoordinates.x < LevelGenerator.Instance.levelSize.x - 1)
                        LevelGenerator.Instance.spawnedModules[moduleCoordinates.x + 1, moduleCoordinates.y].AssignWallIfNotOccupied(new Wall(wall.type, WallPosition.Left));
                    break;
            }
        }
    }

    public List<WallPosition> GetListOfAvailableWalls()
    {
        List<WallPosition> availableWalls = new List<WallPosition>();

        for (int i = 0; i < 4; i++)
        {
            if(!WallPositionIsOccupied((WallPosition)i, false))
                availableWalls.Add((WallPosition)i);
        }

        return availableWalls;
    }

}
