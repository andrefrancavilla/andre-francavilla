using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using Random = UnityEngine.Random;

public class LevelGenerator : MonoBehaviour
{
    private static LevelGenerator _instance;
    public static LevelGenerator Instance
    {
        get
        {
            if (!_instance) _instance = GameObject.FindObjectOfType<LevelGenerator>();
            return _instance;
        }
    }

    public Vector2Int levelSize;
    public GeneratorResources generatorResources;
    public ModuleColumn[] configurationColumn;
    public int closedWallAmount;
    public bool showGizmos;

    public GeneratorMode mode;
    public Module[,] spawnedModules;
    private Vector2Int entranceModuleCoordinates;
    private int maxModuleAccessOrder;
    private List<Vector2Int> pathToWaypoints = new List<Vector2Int>();
    private List<List<Vector2Int>> accessOrderWaypoints = new List<List<Vector2Int>>(); //First indexer refers to order number, second indexer refers to coordinates and it's count indicates the amount
    private List<Vector2Int> userWaypoints = new List<Vector2Int>();
    private List<Vector2Int> randomWaypoints = new List<Vector2Int>();
    private int maxAmountOfWalls;
    public GameObject levelInstance;


    // Start is called before the first frame update
    void Awake()
    {
        _instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        if(mode == GeneratorMode.Runtime)
            Generate();
    }

    public void Generate()
    {
        maxAmountOfWalls = (levelSize.y - 1) * levelSize.x + (levelSize.x - 1) * levelSize.y;
        spawnedModules = new Module[levelSize.x, levelSize.y];
        ModuleColumn[] originalConfigData = new ModuleColumn[configurationColumn.Length];
        for (int x = 0; x < originalConfigData.Length; x++)
        {
            originalConfigData[x] = new ModuleColumn
            {
                configurationRow = new ModuleConfiguration[configurationColumn[x].configurationRow.Length]
            };
            for (int y = 0; y < originalConfigData[x].configurationRow.Length; y++)
            {
                originalConfigData[x].configurationRow[y] = new ModuleConfiguration();
                originalConfigData[x].configurationRow[y].accessOrder = configurationColumn[x].configurationRow[y].accessOrder;
                originalConfigData[x].configurationRow[y].moduleType = configurationColumn[x].configurationRow[y].moduleType;
                originalConfigData[x].configurationRow[y].mustHaveAccess = configurationColumn[x].configurationRow[y].mustHaveAccess;
            }
        }
        
        GameObject generatedLevel = new GameObject {name = "Generated Level"};
        
        if(levelInstance != null)
            DestroyImmediate(levelInstance);
        
        levelInstance = generatedLevel;

        for (int x = 0; x < levelSize.x; x++)
        {
            for(int y = 0; y < levelSize.y; y++)
            {
                //Spawn correct module
                if (configurationColumn[x].configurationRow[levelSize.y - y - 1].moduleType == ModuleType.EasyOrHard)
                {
                    configurationColumn[x].configurationRow[levelSize.y - y - 1].moduleType = Random.Range(1, 100) > 50 ? ModuleType.Hard : ModuleType.Easy;
                }
                
                string currResourcePath = Path.Combine("Prefabs", configurationColumn[x].configurationRow[levelSize.y - y - 1].moduleType.ToString());
                GameObject[] currTypeResources = Resources.LoadAll<GameObject>(currResourcePath);
                
                Debug.Assert(currTypeResources != null);
                
                spawnedModules[x, y] = Instantiate(currTypeResources[Random.Range(0, currTypeResources.Length)], new Vector3(x * generatorResources.moduleSize.x, 0, y * generatorResources.moduleSize.z), Quaternion.identity, generatedLevel.transform).GetComponent<Module>();
                spawnedModules[x, y].moduleCoordinates = new Vector2Int(x, y);

                //Get possible waypoints for obliged paths
                if (configurationColumn[x].configurationRow[y].mustHaveAccess)
                {
                    if (configurationColumn[x].configurationRow[y].moduleType == ModuleType.Entrance)
                    {
                        entranceModuleCoordinates = new Vector2Int(x, levelSize.y - y - 1);
                    }
                    else
                    {
                        if (configurationColumn[x].configurationRow[y].accessOrder > accessOrderWaypoints.Count - 1)
                        {
                            accessOrderWaypoints.Add(new List<Vector2Int>());
                            accessOrderWaypoints[accessOrderWaypoints.Count - 1].Add(new Vector2Int(x, levelSize.y - y - 1));
                        }
                        else
                        {
                            accessOrderWaypoints[configurationColumn[x].configurationRow[y].accessOrder].Add(new Vector2Int(x, levelSize.y - y - 1));
                        }
                    }
                    Debug.Log($"Added waypoint at coordinates {x},{y}");
                        
                    userWaypoints.Add(new Vector2Int(x, levelSize.y - y - 1));
                }
                
                //Place boundary walls (stop player from walking out of matrix boundaries)
                if (x == 0)
                {
                    spawnedModules[x, y].PlaceWall(new Module.Wall(WallType.WallWithoutDoor, WallPosition.Left));
                }
                else if (x == levelSize.x - 1)
                {
                    spawnedModules[x, y].PlaceWall(new Module.Wall(WallType.WallWithoutDoor, WallPosition.Right));
                }

                if (y == 0)
                {
                    spawnedModules[x, y].PlaceWall(new Module.Wall(WallType.WallWithoutDoor, WallPosition.Bottom));
                }
                else if (y == levelSize.y - 1)
                {
                    spawnedModules[x, y].PlaceWall(new Module.Wall(WallType.WallWithoutDoor, WallPosition.Top));
                }
            }
        }
        
        //Add random waypoints
        int maxRandomWaypointAmount = (int)((maxAmountOfWalls - closedWallAmount) / levelSize.magnitude);
        randomWaypoints.Clear();
        for (int i = 0; i < Random.Range(2, maxRandomWaypointAmount); i++)
        {
            Vector2Int randomWaypoint;
            do
            {
                randomWaypoint = new Vector2Int(Random.Range(0, levelSize.x), Random.Range(0, levelSize.y));
            } while (ContainsWaypoint(randomWaypoint));

            accessOrderWaypoints[Random.Range(0, accessOrderWaypoints.Count - 1)].Add(randomWaypoint);
            randomWaypoints.Add(randomWaypoint);

            bool ContainsWaypoint(Vector2Int waypoint)
            {
                for (int i = 0; i < accessOrderWaypoints.Count; i++)
                {
                    if (accessOrderWaypoints[i].Contains(waypoint)) return true;
                }

                return false;
            }
        }

        //If any waypoints have been found in the module configurations, this will draw the obliged path from point A to point B - each waypoint becomes point A when it's reached and the next waypoint becomes point B
        Vector2Int currentPathCoordinates = entranceModuleCoordinates;
        pathToWaypoints.Clear();
        do
        {
            Vector2Int currentTargetWaypoint = NearestWaypoint(currentPathCoordinates);
            do
            {
                if(!pathToWaypoints.Contains(currentPathCoordinates))
                    pathToWaypoints.Add(new Vector2Int(currentPathCoordinates.x, currentPathCoordinates.y));
                
                //Place walls in the direction the player must be allowed to move towards
                if (currentTargetWaypoint.x > currentPathCoordinates.x)
                {
                    spawnedModules[currentPathCoordinates.x, currentPathCoordinates.y].PlaceWall(new Module.Wall(WallType.WallWithDoor, WallPosition.Right));
                    currentPathCoordinates = new Vector2Int(currentPathCoordinates.x + 1, currentPathCoordinates.y);
                }
                else if(currentTargetWaypoint.x < currentPathCoordinates.x)
                {
                        spawnedModules[currentPathCoordinates.x, currentPathCoordinates.y].PlaceWall(new Module.Wall(WallType.WallWithDoor, WallPosition.Left));
                        currentPathCoordinates = new Vector2Int(currentPathCoordinates.x - 1, currentPathCoordinates.y);
                }
                else
                {
                    if (currentTargetWaypoint.y > currentPathCoordinates.y)
                    {
                        spawnedModules[currentPathCoordinates.x, currentPathCoordinates.y].PlaceWall(new Module.Wall(WallType.WallWithDoor, WallPosition.Top));
                        currentPathCoordinates = new Vector2Int(currentPathCoordinates.x, currentPathCoordinates.y + 1);
                    }
                    else if(currentTargetWaypoint.y < currentPathCoordinates.y)
                    {
                        spawnedModules[currentPathCoordinates.x, currentPathCoordinates.y].PlaceWall(new Module.Wall(WallType.WallWithDoor, WallPosition.Bottom));
                        currentPathCoordinates = new Vector2Int(currentPathCoordinates.x, currentPathCoordinates.y - 1);
                    }
                }
            } while (currentPathCoordinates != currentTargetWaypoint);
        } while (accessOrderWaypoints.Count > 0); //Waypoints have multiple access orders, order 0 is top priority
        
        //Place random blocked walls
        for (int i = 0; i < closedWallAmount; i++)
        {
            //HandleAdjacentWalls();
            
            Vector2Int targetedModule;
            List<WallPosition> availableWalls;
            WallPosition position = (WallPosition)Random.Range(0, 4);
            do
            {
                targetedModule = new Vector2Int(Random.Range(0, levelSize.x), Random.Range(0, levelSize.y));
                availableWalls = spawnedModules[targetedModule.x, targetedModule.y].GetListOfAvailableWalls();
                if(availableWalls.Count > 0)
                    position = availableWalls[Random.Range(0, availableWalls.Count)];
                
            } while (availableWalls.Count == 0);
            
            spawnedModules[targetedModule.x, targetedModule.y].PlaceWall(new Module.Wall(WallType.WallWithoutDoor, position));
        }
        
        //place open walls in the remaining modules
        for (int x = 0; x < levelSize.x; x++)
        {
            for (int y = 0; y < levelSize.y; y++)
            {
                List<WallPosition> listOfAvailableWallPositions = spawnedModules[x, y].GetListOfAvailableWalls();
                for (int i = 0; i < listOfAvailableWallPositions.Count; i++)
                {
                    spawnedModules[x, y].PlaceWall(new Module.Wall(WallType.WallWithDoor, listOfAvailableWallPositions[i]));
                }
            }
        }

        configurationColumn = originalConfigData;
    }

    private Vector2Int NearestWaypoint(Vector2Int from)
    {
        Vector2Int nearest = accessOrderWaypoints[0][0];

        if (accessOrderWaypoints[0].Count == 0)
            accessOrderWaypoints.RemoveAt(0);

        for (int i = 0; i < accessOrderWaypoints[0].ToArray().Length; i++) //to array creates a copy to avoid errors
        {
            if (Vector2Int.Distance(from, accessOrderWaypoints[0][i]) < Vector2Int.Distance(from, nearest))
            {
                nearest = accessOrderWaypoints[0][i];
            }
        }

        accessOrderWaypoints[0].Remove(nearest);
        if(accessOrderWaypoints[0].Count == 0)
            accessOrderWaypoints.RemoveAt(0);

        return nearest;
    }

    private void OnDrawGizmos()
    {
        if(levelInstance == null || spawnedModules == null || !showGizmos) return;
        
        Gizmos.color = new Color(1, 0, 0, 0.75f);
        for (int i = 0; i < userWaypoints.Count; i++)
        {
            Gizmos.DrawSphere(spawnedModules[userWaypoints[i].x, userWaypoints[i].y].transform.position - new Vector3(-generatorResources.moduleSize.x / 2f, 0, generatorResources.moduleSize.z / 2f)
                              + Vector3.up * 8, 1);
        }
        for (int i = 0; i < randomWaypoints.Count; i++)
        {
            Gizmos.color = new Color(1, 0.5f, 0, 0.75f);
            Gizmos.DrawSphere(spawnedModules[randomWaypoints[i].x, randomWaypoints[i].y].transform.position - new Vector3(-generatorResources.moduleSize.x / 2f, 0, generatorResources.moduleSize.z / 2f)
                              + Vector3.up * 8, 1);
        }
        for (int i = 0; i < pathToWaypoints.Count; i++)
        {
            Gizmos.color = new Color(1, 0, 1, 0.75f);
            if(i < pathToWaypoints.Count - 1)
                Gizmos.DrawLine(spawnedModules[pathToWaypoints[i].x, pathToWaypoints[i].y].transform.position - new Vector3(-generatorResources.moduleSize.x / 2f, 0, generatorResources.moduleSize.z / 2f)
                              + Vector3.up * 8, spawnedModules[pathToWaypoints[i + 1].x, pathToWaypoints[i + 1].y].transform.position - new Vector3(-generatorResources.moduleSize.x / 2f, 0, generatorResources.moduleSize.z / 2f)
                                                + Vector3.up * 8);
        }
    }

    [System.Serializable]
    public struct ModuleColumn
    {
        public ModuleConfiguration[] configurationRow;
    }
}

[System.Serializable]
public class GeneratorResources
{
    public GameObject wallWithDoor;
    public GameObject wallWithoutDoor;
    public Vector3Int moduleSize = new Vector3Int(10, 1, 10);
}

[System.Serializable]
public struct ModuleConfiguration
{
    [SerializeField] public ModuleType moduleType;
    [SerializeField] public bool mustHaveAccess;
    [SerializeField] public int accessOrder;
}
