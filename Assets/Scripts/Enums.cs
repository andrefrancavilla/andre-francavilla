//This script contains all enums relative to the generator
//It would be a good idea to put all generator-related stuff in a namespace.

public enum WallPosition
{
    Top = 0,
    Bottom = 1,
    Left = 2,
    Right = 3
}

public enum WallType
{
    WallWithDoor,
    WallWithoutDoor
}
[System.Serializable]
public enum ModuleType
{
    Entrance,
    PostEntrance,
    Easy,
    EasyOrHard,
    Hard,
    Loot,
    PreBoss,
    Boss
}

public enum GeneratorMode
{
    Runtime,
    Editor
}