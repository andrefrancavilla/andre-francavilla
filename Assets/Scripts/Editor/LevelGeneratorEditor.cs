using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelGenerator))]
public class LevelGeneratorEditor : Editor
{
    public static LevelGenerator generatorTarget;
    public override void OnInspectorGUI()
    {
        //View classic UI properties
        SerializedObject editorTarget = new SerializedObject(target);
        SerializedProperty resources = editorTarget.FindProperty("generatorResources");
        SerializedProperty generatorMode = editorTarget.FindProperty("mode");
        generatorTarget = target as LevelGenerator;

        GUIStyle style = new GUIStyle();
        style.fontStyle = FontStyle.Bold;
        style.normal.textColor = Color.white;

        EditorGUILayout.PropertyField(generatorMode);

        EditorGUILayout.PropertyField(resources);

        generatorTarget.showGizmos = EditorGUILayout.Toggle("Show gizmos (if available): ", generatorTarget.showGizmos);
        
        EditorGUILayout.Space();

        //Buttons
        if (GUILayout.Button("Configure Level"))
        {
            //Separate window for Editor Configuration (window sets its own pixel size)
            MatrixEditorWindow window = (MatrixEditorWindow) EditorWindow.GetWindow(typeof(MatrixEditorWindow), false, "Matrix Editor");
            window.SetConfigurationMatrixSize();
            window.Show();
        
            Undo.RecordObject(window, "Generator Modification");
            EditorUtility.SetDirty(window);
        }

        if (generatorTarget.mode == GeneratorMode.Editor) //Allows to generate in editor instead of runtime
        {
            if (GUILayout.Button("Generate Level"))
            {
                generatorTarget.Generate();
            }
        }

        //Save
        editorTarget.ApplyModifiedProperties();
    }

    [MenuItem("Tools/Procedural/Create Level Generator", false, 0)]
    public static void CreateLevelGenerator()
    {
        //Tell scene it can save generated level to it
        if (!FindObjectOfType<LevelGenerator>())
        {
            GameObject generator = new GameObject
            {
                name = "Level Generator"
            };
            generator.AddComponent<LevelGenerator>();
            
            EditorUtility.SetDirty(generator);
        }
        else
        {
            Debug.LogError("There already is a generator present in the scene.");
        }
    }
}

class MatrixEditorWindow : EditorWindow
{
    private void OnGUI()
    {
        //Define window style elements
        GUIStyle matrixElementStyle = new GUIStyle(GUI.skin.box);
        GUIStyle centeredLabelStyle = new GUIStyle(GUI.skin.label);
        matrixElementStyle.fixedHeight = 90;
        matrixElementStyle.fixedWidth = 220;
        centeredLabelStyle.alignment = TextAnchor.UpperCenter;
        centeredLabelStyle.fontStyle = FontStyle.Bold;

        if(LevelGeneratorEditor.generatorTarget == null) return; //avoid nullref
        EditorGUILayout.BeginHorizontal();
        LevelGeneratorEditor.generatorTarget.levelSize = EditorGUILayout.Vector2IntField("Matrix Size: ", new Vector2Int(LevelGeneratorEditor.generatorTarget.levelSize.x, LevelGeneratorEditor.generatorTarget.levelSize.y));

        if (GUILayout.Button("Update Matrix", GUILayout.Height(40)))
        {
            SetConfigurationMatrixSize();
        }
        EditorGUILayout.EndHorizontal();
        
        if(LevelGeneratorEditor.generatorTarget.configurationColumn == null) return;

        EditorGUILayout.BeginHorizontal();
        
        //Draw out 2D array of the level on screen
        for (int x = 0; x < LevelGeneratorEditor.generatorTarget.levelSize.x; x++)
        {
            if(LevelGeneratorEditor.generatorTarget.configurationColumn.Length != LevelGeneratorEditor.generatorTarget.levelSize.x) return;
            
            LevelGeneratorEditor.generatorTarget.configurationColumn[x].configurationRow ??= new ModuleConfiguration[LevelGeneratorEditor.generatorTarget.levelSize.y];
            EditorGUILayout.BeginVertical();
            for (int y = 0; y < LevelGeneratorEditor.generatorTarget.levelSize.y; y++)
            {
                if(LevelGeneratorEditor.generatorTarget.configurationColumn[x].configurationRow.Length != LevelGeneratorEditor.generatorTarget.levelSize.y) return;
                
                EditorGUILayout.BeginVertical(matrixElementStyle);
                
                //Array item configuration (applies to all elements in the 2D array)
                EditorGUILayout.LabelField($"[X:{x} Y: {LevelGeneratorEditor.generatorTarget.levelSize.y - y - 1}]", centeredLabelStyle);
                LevelGeneratorEditor.generatorTarget.configurationColumn[x].configurationRow[y].moduleType = (ModuleType) Enum.Parse(typeof(ModuleType), EditorGUILayout.EnumPopup(LevelGeneratorEditor.generatorTarget.configurationColumn[x].configurationRow[y].moduleType).ToString()); 
                LevelGeneratorEditor.generatorTarget.configurationColumn[x].configurationRow[y].mustHaveAccess = EditorGUILayout.Toggle("Is Waypoint: ",LevelGeneratorEditor.generatorTarget.configurationColumn[x].configurationRow[y].mustHaveAccess);
                if (LevelGeneratorEditor.generatorTarget.configurationColumn[x].configurationRow[y].mustHaveAccess)
                {
                    LevelGeneratorEditor.generatorTarget.configurationColumn[x].configurationRow[y].accessOrder = EditorGUILayout.IntField("Waypoint Sort Order", LevelGeneratorEditor.generatorTarget.configurationColumn[x].configurationRow[y].accessOrder, GUILayout.ExpandWidth(false));
                }
                    
                EditorGUILayout.EndVertical();
            }
                
            EditorGUILayout.EndVertical();
        }
        EditorGUILayout.EndHorizontal();
        
        //Max amount is necessary to avoid having too many walls and cause an infinite generation loop.
        int maxAmountOfWalls = ((LevelGeneratorEditor.generatorTarget.levelSize.y - 1) * LevelGeneratorEditor.generatorTarget.levelSize.x +
                                (LevelGeneratorEditor.generatorTarget.levelSize.x - 1) * LevelGeneratorEditor.generatorTarget.levelSize.y)/ 2 - 
                               (LevelGeneratorEditor.generatorTarget.levelSize.x + LevelGeneratorEditor.generatorTarget.levelSize.y) / 2;
        
        LevelGeneratorEditor.generatorTarget.closedWallAmount = EditorGUILayout.IntSlider("Amount of closed walls:", LevelGeneratorEditor.generatorTarget.closedWallAmount, 0, maxAmountOfWalls);
    }

    public void SetConfigurationMatrixSize() //Applies size set in editor to actual matrix configuration
    {
        maxSize = new Vector2(220 * LevelGeneratorEditor.generatorTarget.levelSize.x + 40, 90 * LevelGeneratorEditor.generatorTarget.levelSize.y + 90);
        minSize = new Vector2(220 * LevelGeneratorEditor.generatorTarget.levelSize.x + 40, 90 * LevelGeneratorEditor.generatorTarget.levelSize.y + 90);
        
        //Currently when size changes, the previous configuration is lost. Further improvements can be done here, copying array values (not references!)
        if (LevelGeneratorEditor.generatorTarget.configurationColumn == null)
        {
            LevelGeneratorEditor.generatorTarget.configurationColumn = new LevelGenerator.ModuleColumn[LevelGeneratorEditor.generatorTarget.levelSize.x];
            for (int i = 0; i < LevelGeneratorEditor.generatorTarget.configurationColumn.Length; i++)
            {
                LevelGeneratorEditor.generatorTarget.configurationColumn[i].configurationRow = new ModuleConfiguration[LevelGeneratorEditor.generatorTarget.levelSize.y];
            }
        }
        else if (LevelGeneratorEditor.generatorTarget.configurationColumn.Length != LevelGeneratorEditor.generatorTarget.levelSize.x)
        {
            LevelGeneratorEditor.generatorTarget.configurationColumn = new LevelGenerator.ModuleColumn[LevelGeneratorEditor.generatorTarget.levelSize.x];
            for (int i = 0; i < LevelGeneratorEditor.generatorTarget.configurationColumn.Length; i++)
            {
                LevelGeneratorEditor.generatorTarget.configurationColumn[i].configurationRow = new ModuleConfiguration[LevelGeneratorEditor.generatorTarget.levelSize.y];
            }
        }
        else
        {
            if (LevelGeneratorEditor.generatorTarget.configurationColumn[0].configurationRow.Length == LevelGeneratorEditor.generatorTarget.levelSize.y) return;
            
            for (int i = 0; i < LevelGeneratorEditor.generatorTarget.configurationColumn.Length; i++)
            {
                LevelGeneratorEditor.generatorTarget.configurationColumn[i].configurationRow = new ModuleConfiguration[LevelGeneratorEditor.generatorTarget.levelSize.y];
            }
        }
    }
}
